import java.rmi.Remote;
import java.rmi.RemoteException;


public interface IServidor extends Remote {

	public String executarTarefa(String argumentos) throws RemoteException;
	
}//fim interface
