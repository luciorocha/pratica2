
Exemplo simples de intera��o entre Cliente e Servidor RMI
---------------------------------------------------------

Autor: Lucio Agostinho Rocha
Data: 09/06/2008

Arquivos necess�rios para o lado servidor:
- Servidor.class
- IServidor.class

Arquivos necess�rios para o lado cliente:
- Cliente.class
- IServidor.class

Esse exemplo considera que exista uma interface comum (IServidor.class)
que define quais m�todos do objeto servidor podem ser acessados remotamente
pelo objeto cliente

Os arquivos cliente.policy e servidor.policy definem as permiss�es de seguran�a
necess�rias para o acesso remoto. 

------------
Execu��o:

SERVIDOR:
java -cp . -Djava.security.policy=/root/rmi/servidor.policy Servidor

CLIENTE:
java -cp . -Djava.security.policy=/root/rmi/cliente.policy Cliente NOME_COMPUTADOR_REMOTO ARGUMENTO

(Ex.: java -cp . -Djava.security.policy=/root/rmi/cliente.policy Cliente meu_notebook 3)
